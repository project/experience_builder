import { useState, useEffect } from 'react';
import { Flex, TextField } from '@radix-ui/themes';
import { useUpdateCodeComponentMutation } from '@/services/componentAndLayout';
import { useAppDispatch, useAppSelector } from '@/app/hooks';
import {
  closeAllDialogs,
  selectDialogStates,
  selectSelectedCodeComponent,
} from '@/features/ui/codeComponentDialogSlice';
import {
  selectId as selectCodeEditorId,
  setName as setCodeEditorName,
} from '@/features/code-editor/codeEditorSlice';
import Dialog, { DialogFieldLabel } from '@/components/Dialog';

const RenameCodeComponentDialog = () => {
  const selectedComponent = useAppSelector(selectSelectedCodeComponent);
  const codeEditorId = useAppSelector(selectCodeEditorId);
  const [componentName, setComponentName] = useState('');
  const [updateCodeComponent, { isLoading, isSuccess, isError, error, reset }] =
    useUpdateCodeComponentMutation();
  const dispatch = useAppDispatch();
  const { isRenameDialogOpen } = useAppSelector(selectDialogStates);

  useEffect(() => {
    if (selectedComponent) {
      setComponentName(selectedComponent.name);
    }
  }, [selectedComponent]);

  const handleSave = async () => {
    if (!selectedComponent) return;

    await updateCodeComponent({
      id: selectedComponent.machineName,
      changes: {
        // @todo: Only send wanted changes in the PATCH request when
        //   https://www.drupal.org/project/experience_builder/issues/3508140 is fixed.
        ...selectedComponent,
        name: componentName,
      },
    });
    if (codeEditorId === selectedComponent.machineName) {
      dispatch(setCodeEditorName(componentName));
    }
  };

  const handleOpenChange = (open: boolean) => {
    if (!open) {
      setComponentName('');
      reset();
      dispatch(closeAllDialogs());
    }
  };

  useEffect(() => {
    if (isSuccess) {
      setComponentName('');
      dispatch(closeAllDialogs());
    }
  }, [isSuccess, dispatch]);

  useEffect(() => {
    if (isError) {
      console.error('Failed to rename component:', error);
    }
  }, [isError, error]);

  return (
    <Dialog
      open={isRenameDialogOpen}
      onOpenChange={handleOpenChange}
      title="Rename component"
      error={
        isError
          ? {
              title: 'Failed to rename component',
              message: `An error ${
                'status' in error ? '(HTTP ' + error.status + ')' : ''
              } occurred while renaming the component. Please check the browser console for more details.`,
              resetButtonText: 'Try again',
              onReset: handleSave,
            }
          : undefined
      }
      footer={{
        cancelText: 'Cancel',
        confirmText: 'Rename',
        onConfirm: handleSave,
        isConfirmDisabled:
          !componentName.trim() || componentName === selectedComponent?.name,
        isConfirmLoading: isLoading,
      }}
    >
      <Flex direction="column" gap="2">
        <DialogFieldLabel htmlFor={'componentName'}>
          Component name
        </DialogFieldLabel>
        <TextField.Root
          id={'componentName'}
          value={componentName}
          onChange={(e) => setComponentName(e.target.value)}
          placeholder="Enter a new name"
          size="1"
        />
      </Flex>
    </Dialog>
  );
};

export default RenameCodeComponentDialog;
