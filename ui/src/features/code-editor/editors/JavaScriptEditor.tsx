import CodeMirror from '@uiw/react-codemirror';
import { javascript } from '@codemirror/lang-javascript';
import { githubLight } from '@uiw/codemirror-theme-github';
import { useAppDispatch, useAppSelector } from '@/app/hooks';
import {
  selectSourceCodeJs,
  setSourceCodeJs,
} from '@/features/code-editor/codeEditorSlice';

const JavaScriptEditor = ({ isLoading }: { isLoading: boolean }) => {
  const dispatch = useAppDispatch();
  const value = useAppSelector(selectSourceCodeJs);

  function onChangeHandler(value: string) {
    dispatch(setSourceCodeJs(value));
  }
  if (isLoading) {
    return null;
  }
  return (
    <CodeMirror
      className="xb-code-mirror-editor"
      value={value}
      onChange={onChangeHandler}
      theme={githubLight}
      extensions={[javascript({ jsx: true })]}
    />
  );
};

export default JavaScriptEditor;
