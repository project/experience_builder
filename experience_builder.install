<?php

declare(strict_types=1);

use Drupal\Core\Extension\ThemeInstallerInterface;
use Drupal\experience_builder\AutoSave\AutoSaveManager;

/**
 * Implements hook_install().
 */
function experience_builder_install(): void {
  // Ensure xb_stark is installed for the semi-coupled theme engine.
  // @see \Drupal\experience_builder\Theme\XBThemeNegotiator
  // @see \Drupal\Core\Theme\ThemeNegotiator::determineActiveTheme()
  \Drupal::service(ThemeInstallerInterface::class)->install(['xb_stark']);

  // Cache flush ensures the contextual form works.
  drupal_flush_all_caches();
}

/**
 * Implements hook_module_preinstall().
 */
function experience_builder_module_preinstall(string $module): void {
  if ($module !== 'experience_builder') {
    return;
  }

  // Register the JSON schema definitions stream wrapper *prior* to installing
  // default config and hence prior to config validation. Otherwise validation
  // fails.
  // @see \Drupal\experience_builder\JsonSchemaDefinitionsStreamwrapper
  // @see \Drupal\Core\Extension\ModuleInstaller::install()
  // @todo Remove this after https://www.drupal.org/project/drupal/issues/3352063 lands.
  \Drupal::service('stream_wrapper_manager')->register();
}

/**
 * Implements hook_module_preuninstall().
 */
function experience_builder_module_preuninstall(string $module, bool $is_syncing): void {
  if ($module !== 'experience_builder') {
    return;
  }

  \Drupal::service(AutoSaveManager::class)->deleteAll();
}

/**
 * Implements hook_requirements().
 *
 * @todo Remove when https://www.drupal.org/project/drupal/issues/3472008 is fixed in Drupal core.
 */
function experience_builder_requirements(string $phase): array {
  $requirements = [];
  if ($phase === 'install'|| $phase == 'runtime') {
    // Check if JSON:API module is enabled.
    $json_api_enabled = \Drupal::moduleHandler()->moduleExists('jsonapi');
    if ($json_api_enabled) {
      $severity = REQUIREMENT_WARNING;
      // Check if PHP assertions are enabled and update the severity.
      // @see \Drupal\experience_builder\EventSubscriber\ApiMessageValidator::isProd()
      $assertions_disabled = TRUE;
      // @phpstan-ignore booleanNot.alwaysTrue
      assert(!($assertions_disabled = FALSE));
      // @phpstan-ignore booleanNot.alwaysTrue
      if (!$assertions_disabled) {
        $severity = REQUIREMENT_ERROR;
      }
      $requirements['experience_builder'] = [
        'title' => t('Experience Builder'),
        'description' => t("Experience Builder requires the <code>justinrainbow/json-schema</code> library which may impact site performance when the JSON:API module is installed and PHP assertions are enabled. See <a href=':url'>:url</a> for more information.", [':url' => 'https://www.drupal.org/project/drupal/issues/3472008']),
        'severity' => $severity,
      ];
    }
  }
  return $requirements;
}
