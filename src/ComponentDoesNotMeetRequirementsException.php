<?php

declare(strict_types=1);

namespace Drupal\experience_builder;

/**
 * Defines an exception for when a component doesn't meet requirements.
 */
final class ComponentDoesNotMeetRequirementsException extends \Exception {

}
