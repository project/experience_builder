<?php

declare(strict_types=1);

namespace Drupal\experience_builder\Plugin\ExperienceBuilder\ComponentSource;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Entity\TypedData\EntityDataDefinition;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItemInterface;
use Drupal\Core\Field\WidgetPluginManager;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\Component as SdcPlugin;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Component\Exception\ComponentNotFoundException;
use Drupal\Core\Render\Component\Exception\InvalidComponentException;
use Drupal\Core\StreamWrapper\PublicStream;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Theme\Component\ComponentValidator;
use Drupal\experience_builder\ComponentSource\ComponentSourceBase;
use Drupal\experience_builder\ComponentSource\ComponentSourceWithSlotsInterface;
use Drupal\experience_builder\Entity\Component as ComponentEntity;
use Drupal\experience_builder\InvalidRequestBodyValue;
use Drupal\experience_builder\MissingHostEntityException;
use Drupal\experience_builder\Plugin\Field\FieldType\ComponentTreeItem;
use Drupal\experience_builder\PropExpressions\Component\ComponentPropExpression;
use Drupal\experience_builder\PropExpressions\StructuredData\FieldObjectPropsExpression;
use Drupal\experience_builder\PropExpressions\StructuredData\FieldPropExpression;
use Drupal\experience_builder\PropExpressions\StructuredData\FieldTypeObjectPropsExpression;
use Drupal\experience_builder\PropExpressions\StructuredData\ReferenceFieldPropExpression;
use Drupal\experience_builder\PropExpressions\StructuredData\ReferenceFieldTypePropExpression;
use Drupal\experience_builder\PropShape\PropShape;
use Drupal\experience_builder\PropShape\StorablePropShape;
use Drupal\experience_builder\PropSource\UrlPreviewPropSource;
use Drupal\experience_builder\PropSource\PropSource;
use Drupal\experience_builder\PropSource\StaticPropSource;
use Drupal\experience_builder\ShapeMatcher\FieldForComponentSuggester;
use Drupal\media\Entity\MediaType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * Explicit input UX generated from SDC metadata, using field types and widgets.
 *
 * XB ComponentSource plugins that do not have their own (native) explicit
 * input UX only need to map their explicit information to SDC metadata and can
 * then get an automatically generated field widget explicit UX, whose values
 * are stored in dangling field instances, by mapping schema to field types.
 *
 * @see \Drupal\Core\Theme\Component\ComponentMetadata
 * @see \Drupal\experience_builder\ShapeMatcher\SdcPropToFieldTypePropMatcher
 *
 * Component Source plugins included in the Experience Builder module using it:
 * - "SDC"
 * - "code components"
 *
 * @see \Drupal\experience_builder\Plugin\ExperienceBuilder\ComponentSource\SingleDirectoryComponent
 * @see \Drupal\experience_builder\Plugin\ExperienceBuilder\ComponentSource\JsComponent
 *
 * @internal
 */
abstract class GeneratedFieldExplicitInputUxComponentSourceBase extends ComponentSourceBase implements ComponentSourceWithSlotsInterface, ContainerFactoryPluginInterface {

  public const EXPLICIT_INPUT_NAME = 'props';

  public function __construct(
    array $configuration,
    string $plugin_id,
    array $plugin_definition,
    private readonly ComponentValidator $componentValidator,
    private readonly FieldTypePluginManagerInterface $fieldTypePluginManager,
    private readonly WidgetPluginManager $fieldWidgetPluginManager,
    private readonly FieldForComponentSuggester $fieldForComponentSuggester,
    protected readonly EntityTypeManagerInterface $entityTypeManager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // @phpstan-ignore-next-line
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get(ComponentValidator::class),
      $container->get(FieldTypePluginManagerInterface::class),
      $container->get('plugin.manager.field.widget'),
      $container->get(FieldForComponentSuggester::class),
      $container->get(EntityTypeManagerInterface::class),
    );
  }

  /**
   * The SDC metadata that everything else in this trait builds upon.
   *
   * @todo Refactor to only need ComponentMetadata, but that requires refactoring XB's shape matching infrastructure
   *   as well as core's component validator.
   * @see \Drupal\Core\Theme\Component\ComponentMetadata
   * @see \Drupal\experience_builder\PropShape\PropShape::getComponentProps()
   */
  abstract protected function getSdcPlugin(): SdcPlugin;

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies(): array {
    assert(array_key_exists('prop_field_definitions', $this->configuration));
    assert(is_array($this->configuration['prop_field_definitions']));
    $dependencies = [];
    foreach ($this->configuration['prop_field_definitions'] as ['field_type' => $field_type, 'field_widget' => $field_widget]) {
      // TRICKY: `field_type` (and `field_widget`) may not be set if no field
      // types match this SDC prop shape.
      if ($field_type === NULL) {
        continue;
      }
      $field_type_definition = $this->fieldTypePluginManager->getDefinition($field_type);
      $dependencies['module'][] = $field_type_definition['provider'];
      $field_widget_definition = $this->fieldWidgetPluginManager->getDefinition($field_widget);
      $dependencies['module'][] = $field_widget_definition['provider'];
    }

    return $dependencies;
  }

  /**
   * Build the default prop source for a prop.
   *
   * @param string $prop_name
   *   The prop name.
   *
   * @return \Drupal\experience_builder\PropSource\StaticPropSource
   *   The prop source object.
   */
  private function getDefaultStaticPropSource(string $prop_name): StaticPropSource {
    assert(isset($this->configuration['prop_field_definitions']));
    assert(is_array($this->configuration['prop_field_definitions']));
    $component_schema = $this->getSdcPlugin()->metadata->schema ?? [];
    if (!array_key_exists($prop_name, $component_schema['properties'] ?? [])) {
      throw new \OutOfRangeException(sprintf("'%s' is not a prop on the component '%s'.", $prop_name, $this->getComponentDescription()));
    }

    $sdc_prop_source = [
      'sourceType' => 'static:field_item:' . $this->configuration['prop_field_definitions'][$prop_name]['field_type'],
      'value' => $this->configuration['prop_field_definitions'][$prop_name]['default_value'],
      'expression' => $this->configuration['prop_field_definitions'][$prop_name]['expression'],
    ];
    if (array_key_exists('field_storage_settings', $this->configuration['prop_field_definitions'][$prop_name])) {
      $sdc_prop_source['sourceTypeSettings']['storage'] = $this->configuration['prop_field_definitions'][$prop_name]['field_storage_settings'];
    }
    if (array_key_exists('field_instance_settings', $this->configuration['prop_field_definitions'][$prop_name])) {
      $sdc_prop_source['sourceTypeSettings']['instance'] = $this->configuration['prop_field_definitions'][$prop_name]['field_instance_settings'];
    }

    return StaticPropSource::parse($sdc_prop_source);
  }

  public function getSlotDefinitions(): array {
    return $this->getSdcPlugin()->metadata->slots;
  }

  /**
   * {@inheritdoc}
   */
  public function getExplicitInput(string $uuid, ComponentTreeItem $item): array {
    if (!$this->requiresExplicitInput()) {
      return [];
    }
    $entity = $item->getRoot() === $item ? NULL : $item->getEntity();
    $values = $item->get('inputs')->getValues($uuid);
    return array_map(
      // @phpstan-ignore-next-line
      fn(array $prop_source): mixed => PropSource::parse($prop_source)
        ->evaluate($entity),
      $values,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function hydrateComponent(array $explicit_input): array {
    $hydrated[self::EXPLICIT_INPUT_NAME] = $explicit_input;

    if ($slots = $this->getSlotDefinitions()) {
      // Use the first example defined in SDC metadata, if it exists. Otherwise,
      // fall back to `"#plain_text => ''`, which is accepted by SDC's rendering
      // logic but still results in an empty slot.
      // @see https://www.drupal.org/node/3391702
      // @see \Drupal\Core\Render\Element\ComponentElement::generateComponentTemplate()
      $hydrated['slots'] = array_map(fn($slot) => $slot['examples'][0] ?? '', $slots);
    }

    return $hydrated;
  }

  /**
   * {@inheritdoc}
   */
  public function inputToClientModel(array $explicit_input): array {
    // @see PropSourceComponent type-script definition.
    // @see EvaluatedComponentModel type-script definition.
    $model = [
      'resolved' => $explicit_input,
      'source' => [],
    ];

    foreach ($explicit_input as $prop_name => $value) {
      $prop_source = $this->getDefaultStaticPropSource($prop_name);
      $source = $prop_source->toArray();
      try {
        // @todo To support DynamicPropSource, \Drupal\experience_builder\ComponentSource\ComponentSourceInterface::inputToClientModel() will need to be updated to allow a host entity to be passed in.
        $value = $prop_source->evaluate(NULL);
      }
      catch (\OutOfRangeException) {
        // This was a dynamic prop source, but is missing the data it needs.
        // Try to fall-back to the default value.
        $value = \array_key_exists('value', $source) ? $source['value'] : NULL;
      }
      // Don't duplicate value if the resolved value matches the static value.
      // TRICKY: it's thanks to the condition in this if-branch NOT being met
      // that it's possible for the preview ('resolved') to not match the input
      // ('source'): the source will retain its own value, even if that is the
      // empty array in for example the case of a default image.
      if (\array_key_exists('value', $source) && $value === $source['value']) {
        unset($source['value']);
      }
      $model['source'][$prop_name] = $source;
    }

    return $model;
  }

  /**
   * {@inheritdoc}
   */
  public function requiresExplicitInput(): bool {
    return !empty($this->getSdcPlugin()->metadata->schema['properties']);
  }

  /**
   * {@inheritdoc}
   */
  public function validateComponentInput(array $inputValues, string $component_instance_uuid, ?FieldableEntityInterface $entity): ConstraintViolationListInterface {
    $violations = new ConstraintViolationList();
    foreach ($inputValues as $component_prop_name => $raw_prop_source) {
      if (str_starts_with($raw_prop_source['sourceType'], 'static:')) {
        try {
          StaticPropSource::isMinimalRepresentation($raw_prop_source);
        }
        catch (\LogicException $e) {
          $violations->add(new ConstraintViolation(
            sprintf("For component `%s`, prop `%s`, an invalid field property value was detected: %s.",
              $component_instance_uuid,
              $component_prop_name,
              $e->getMessage()),
            NULL,
            [],
            $entity,
            "inputs.$component_instance_uuid.$component_prop_name",
            $raw_prop_source,
          ));
        }
      }
    }
    try {
      $resolvedInputValues = array_map(
      // @phpstan-ignore-next-line
        fn(array $prop_source): mixed => PropSource::parse($prop_source)
          ->evaluate($entity),
        $inputValues,
      );
    }
    catch (MissingHostEntityException $e) {
      // DynamicPropSources cannot be validated in isolation, only in the
      // context of a host content entity.
      if ($entity === NULL) {
        // This case can only be hit when using a DynamicPropSource
        // inappropriately, which is validated elsewhere.
        // @see \Drupal\experience_builder\Plugin\Validation\Constraint\ComponentTreeMeetsRequirementsConstraintValidator
        return $violations;
      }
      // Some component inputs (SDC props) may not be resolvable yet because\
      // required fields do not yet have values specified.
      // @see https://www.drupal.org/project/drupal/issues/2820364
      // @see \Drupal\experience_builder\Plugin\Field\FieldType\ComponentTreeItem::postSave()
      elseif ($entity->isNew()) {
        // Silence this exception until the required field is populated.
        return $violations;
      }
      else {
        // The required field must be populated now (this branch can only be
        // hit when the entity already exists and hence all required fields
        // must have values already), so do not silence the exception.
        throw $e;
      }
    }

    try {
      $this->componentValidator->validateProps($resolvedInputValues, $this->getSdcPlugin());
    }
    catch (ComponentNotFoundException) {
      // The violation for a missing component will be added in the validation
      // of the tree structure.
      // @see \Drupal\experience_builder\Plugin\Validation\Constraint\ComponentTreeStructureConstraintValidator
    }
    catch (InvalidComponentException $e) {
      // Deconstruct the multi-part exception message constructed by SDC.
      // @see \Drupal\Core\Theme\Component\ComponentValidator::validateProps()
      $errors = explode("\n", $e->getMessage());
      foreach ($errors as $error) {
        // An example error:
        // @code
        // [style] Does not have a value in the enumeration ["primary","secondary"]
        // @endcode
        // In that string, `[style]` is the bracket-enclosed SDC prop name
        // for which an error occurred. This string must be parsed.
        $sdc_prop_name_closing_bracket_pos = strpos($error, ']', 1);
        assert($sdc_prop_name_closing_bracket_pos !== FALSE);
        // This extracts `style` and the subsequent error message from the
        // example string above.
        $prop_name = substr($error, 1, $sdc_prop_name_closing_bracket_pos - 1);
        $prop_error_message = substr($error, $sdc_prop_name_closing_bracket_pos + 2);

        if (\version_compare(\Drupal::VERSION, '11.1.2', '>=') && \str_contains($prop_name, '/')) {
          // From Drupal 11.1.2 the exception message also includes the component ID.
          // @see https://drupal.org/i/3462700
          [, $prop_name] = \explode('/', $prop_name);
        }
        $violations->add(
          new ConstraintViolation(
            $prop_error_message,
            NULL,
            [],
            $entity,
            "inputs.$component_instance_uuid.$prop_name",
            $resolvedInputValues[$prop_name] ?? NULL,
          )
        );
      }
    }
    return $violations;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state,
    string $component_instance_uuid = '',
    array $client_model = [],
    ?EntityInterface $entity = NULL,
    array $settings = [],
  ): array {
    assert($entity instanceof FieldableEntityInterface);
    $component_schema = $this->getSdcPlugin()->metadata->schema ?? [];

    // Allow form alterations specific to XB component inputs forms (currently
    // only "static prop sources").
    $form_state->set('is_xb_static_prop_source', TRUE);

    $prop_field_definitions = $settings['prop_field_definitions'];

    $component = $form['#component'];
    \assert($component instanceof ComponentEntity);
    // To ensure the order of the fields always matches the order of the schema
    // we loop over the properties from the schema, but first we have to
    // exclude props that aren't storable.
    $component_plugin = $this->getSdcPlugin();
    $storable_props = [];

    foreach (PropShape::getComponentProps($component_plugin) as $component_prop_expression => $prop_shape) {
      $storable_prop_shape = $prop_shape->getStorage();
      // @todo Remove this once every SDC prop shape can be stored. See PropShapeRepositoryTest::getExpectedUnstorablePropShapes()
      // @todo Create a status report that lists which SDC prop shapes are not storable.
      if (!$storable_prop_shape) {
        continue;
      }
      $component_prop = ComponentPropExpression::fromString($component_prop_expression);
      $storable_props[] = $component_prop->propName;
    }
    foreach ($storable_props as $sdc_prop_name) {
      $prop_source_array = $client_model[$sdc_prop_name] ?? NULL;
      if ($prop_source_array === NULL) {
        // The client didn't send this prop but should. This is an error OR the
        // data has been tampered with.
        throw HttpException::fromStatusCode(Response::HTTP_BAD_REQUEST);
      }
      $source = PropSource::parse($prop_source_array);
      $disabled = FALSE;
      if (!$source instanceof StaticPropSource) {
        // @todo Design is undefined for the DynamicPropSource UX. Related: https://www.drupal.org/project/experience_builder/issues/3459234
        // @todo Design is undefined for the AdaptedPropSource UX.
        // Fall back to the static version, disabled for now where the design is undefined.
        $disabled = !$source instanceof UrlPreviewPropSource;
        $source = $this->getDefaultStaticPropSource($sdc_prop_name)->withValue($prop_source_array['value'] ?? NULL);
      }
      // 1. If the given static prop source matches the *current* field type
      // configuration, use the configured widget.
      // 2. Worst case: fall back to the default widget for this field type.
      // @todo Implement 2. in https://www.drupal.org/project/experience_builder/issues/3463996
      $field_widget_plugin_id = NULL;
      if ($source->getSourceType() === 'static:field_item:' . $prop_field_definitions[$sdc_prop_name]['field_type']) {
        $field_widget_plugin_id = $prop_field_definitions[$sdc_prop_name]['field_widget'];
      }
      assert(isset($component_schema['properties'][$sdc_prop_name]['title']));
      $label = $component_schema['properties'][$sdc_prop_name]['title'];
      $is_required = isset($component_schema['required']) && in_array($sdc_prop_name, $component_schema['required'], TRUE);
      $form[$sdc_prop_name] = $source->formTemporaryRemoveThisExclamationExclamationExclamation($field_widget_plugin_id, $sdc_prop_name, $label, $is_required, $entity, $form, $form_state);
      $form[$sdc_prop_name]['#disabled'] = $disabled;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
    // @todo Implementation.
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    // @todo Implementation.
  }

  /**
   * {@inheritdoc}
   */
  public function getClientSideInfo(ComponentEntity $component): array {
    $component_plugin = $this->getSdcPlugin();
    assert($component_plugin instanceof SdcPlugin);
    // @todo Refactor away this instanceof check in https://www.drupal.org/i/3503038
    $suggestions = $this instanceof SingleDirectoryComponent
      ? $this->fieldForComponentSuggester->suggest($component_plugin->getPluginId(), EntityDataDefinition::create('node', 'article'))
      : [];
    $prop_field_definitions = $component->getSettings()['prop_field_definitions'];

    $field_data = [];
    $default_props_for_default_markup = [];
    $unpopulated_props_for_default_markup = [];
    $dynamic_prop_source_candidates = [];
    $transforms = [];
    foreach (PropShape::getComponentProps($component_plugin) as $component_prop_expression => $prop_shape) {
      $storable_prop_shape = $prop_shape->getStorage();
      // @todo Remove this once every SDC prop shape can be stored. See PropShapeRepositoryTest::getExpectedUnstorablePropShapes()
      // @todo Create a status report that lists which SDC prop shapes are not storable.
      if (!$storable_prop_shape) {
        continue;
      }

      $component_prop = ComponentPropExpression::fromString($component_prop_expression);
      $prop_name = $component_prop->propName;
      if (isset($suggestions[$component_prop_expression])) {
        $dynamic_prop_source_candidates[$prop_name] = array_map(
          fn(FieldPropExpression|FieldObjectPropsExpression|ReferenceFieldPropExpression $expr) => (string) $expr,
          $suggestions[$component_prop_expression]['instances']
        );
      }

      // Determine the default:
      // - resolved value (used for the preview of the component)
      // - source value (used to populate
      // Typically, they are different representations of the same value:
      // - resolved: value conforming to an SDC prop shape
      // - source: value as stored by the corresponding storable prop shape, so
      //   in an instance of a field type, which can either be a single field
      //   prop (for field types with a single property) or an array of field
      //   props (for field types with >1 properties)
      // @see \Drupal\experience_builder\PropShape\PropShape
      // @see \Drupal\experience_builder\PropShape\StorablePropShape
      // @see \Drupal\Core\Field\FieldItemInterface::propertyDefinitions()
      // @see ::exampleValueRequiresEntity()
      // @todo https://www.drupal.org/project/experience_builder/issues/3493943 will introduce the same 'resolved' vs 'source' split for *default values* that https://www.drupal.org/project/experience_builder/issues/3493941 already introduced for *user-entered values*. This code leaps ahead of that.

      // Inspect the Component config entity to check for the presence of a
      // default value.
      // Defaults are guaranteed to exist for required props, may exist for
      // optional props. When an optional prop has no default value, the value
      // stored as the default in the Component config entity is NULL.
      // @see \Drupal\experience_builder\ComponentMetadataRequirementsChecker
      assert(self::exampleValueRequiresEntity($storable_prop_shape) === ($this->configuration['prop_field_definitions'][$prop_name]['default_value'] === []));
      $default_source_value = $this->configuration['prop_field_definitions'][$prop_name]['default_value'];
      $has_default_source_value = match ($default_source_value) {
        // NULL is stored to signal this is an optional SDC prop without an
        // example value.
        NULL => FALSE,
        // The empty array is stored to signal this is an SDC prop (optional or
        // required) whose example value would need an entity to be created,
        // which is not allowed.
        // @see ::exampleValueRequiresEntity()
        [] => FALSE,
        // In all other cases, a default value is present.
        default => TRUE,
      };

      // Compute the default 'resolved' value, which will be used to:
      // - generate the preview of the component
      // - populate the client-side (data) `model`
      // … which in both cases boils down to: "this value is passed directly
      // into the SDC".
      $default_resolved_value = NULL;
      // Use the stored default, if any. This is required for all required SDC
      // props, optional for all optional SDC props.
      if ($has_default_source_value) {
        $default_resolved_value = $this->getDefaultStaticPropSource($prop_name)->evaluate(NULL);
      }
      // One special case: example values that require a Drupal entity to
      // exist. In these cases (for either required or optional SDC props),
      // fall back to the literal example value in the SDC.
      elseif (self::exampleValueRequiresEntity($storable_prop_shape)) {
        // An example may be present in the SDC metadata, it just cannot be
        // mapped to a default value in the prop source.
        if (isset($component_plugin->metadata->schema['properties'][$prop_name]['examples'][0])) {
          $default_resolved_value = $component_plugin->metadata->schema['properties'][$prop_name]['examples'][0];
        }
      }

      // Collect the 'resolved' values for all SDC props, to generate a preview
      // ("default markup").
      if ($default_resolved_value !== NULL) {
        $default_props_for_default_markup[$prop_name] = $default_resolved_value;
      }
      // Track those SDC props without a 'resolved' value (because an example
      // value is missing, which is allowed for optional SDC props), because it
      // will still be necessary to generate the necessary 'source' information
      // for them (to send to ComponentInputsForm).
      else {
        $unpopulated_props_for_default_markup[$prop_name] = NULL;
      }

      // Gather the information that the client will pass to the server to
      // generate a form.
      // @see \Drupal\experience_builder\Form\ComponentInputsForm
      $field_data[$prop_name] = [
        'required' => in_array($prop_name, $component_plugin->metadata->schema['required'] ?? [], TRUE),
        'jsonSchema' => $prop_shape->resolvedSchema,
      ];
      if ($has_default_source_value) {
        $field_data[$prop_name]['default_values'] = $default_source_value;
      }

      // Build transforms from widget metadata.
      $field_widget_plugin_id = NULL;
      $static_prop_source = $storable_prop_shape->toStaticPropSource();
      $prop_field_definition = $prop_field_definitions[$prop_name];
      if ($static_prop_source->getSourceType() === 'static:field_item:' . $prop_field_definition['field_type']) {
        $field_widget_plugin_id = $prop_field_definition['field_widget'];
      }
      if ($field_widget_plugin_id === NULL) {
        continue;
      }
      $widget_definition = $this->fieldWidgetPluginManager->getDefinition($field_widget_plugin_id);
      if (\array_key_exists('xb', $widget_definition) && \array_key_exists('transforms', $widget_definition['xb'])) {
        $transforms[$prop_name] = $widget_definition['xb']['transforms'];
      }
      else {
        throw new \LogicException(sprintf(
          "Experience Builder determined the `%s` field widget plugin must be used to populate the `%s` prop on the `%s` component. However, no `xb.transforms` metadata is defined on the field widget plugin definition. This makes it impossible for this widget to work. Please define the missing metadata. See %s for guidance.",
          $field_widget_plugin_id,
          $component_prop->componentName,
          $component_prop->propName,
          'https://git.drupalcode.org/project/experience_builder/-/raw/0.x/experience_builder.api.php?ref_type=heads',
        ));
      }
    }

    // Avoid duplicating logic: reuse ::inputToClientModel(), add missing data.
    // Note how ::inputToClientModel() returns a shape for the client's
    // `EvaluatedComponentModel`. Its 'source' key is almost identical to the
    // client's `FieldData`: keys are prop names, values are `StaticPropSource`s
    // and *that* overlaps significantly with `FieldDataItem` (which allows
    // arbitrary additional keys).
    // @see EvaluatedComponentModel type-script definition.
    // @see FieldData type-script definition.
    // @see StaticPropSource type-script definition.
    $field_data = NestedArray::mergeDeep(
      $field_data,
      $this->inputToClientModel($default_props_for_default_markup)['source'],
      $this->inputToClientModel($unpopulated_props_for_default_markup)['source'],
    );

    return [
      'source' => (string) $this->getSourceLabel(),
      'build' => $this->renderComponent([self::EXPLICIT_INPUT_NAME => $default_props_for_default_markup], $component->uuid()),
      // Additional data only needed for SDCs.
      // @todo UI does not use any other metadata - should `slots` move to top level?
      'metadata' => ['slots' => $this->getSlotDefinitions()],
      'field_data' => $field_data,
      'dynamic_prop_source_candidates' => $dynamic_prop_source_candidates,
      'transforms' => $transforms,
    ];
  }

  /**
   * Returns the source label for this component.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The source label.
   */
  abstract protected function getSourceLabel(): TranslatableMarkup;

  /**
   * Build the prop settings for an SDC component.
   *
   * @param \Drupal\Core\Plugin\Component $component_plugin
   *   The SDC component.
   *
   * @return array
   *   The prop settings.
   */
  public static function getPropsForComponentPlugin(SdcPlugin $component_plugin): array {
    $props = [];
    foreach (PropShape::getComponentProps($component_plugin) as $cpe_string => $prop_shape) {
      $cpe = ComponentPropExpression::fromString($cpe_string);

      assert(is_array($component_plugin->metadata->schema));
      // @see https://json-schema.org/understanding-json-schema/reference/object#required
      // @see https://json-schema.org/learn/getting-started-step-by-step#required
      $is_required = in_array($cpe->propName, $component_plugin->metadata->schema['required'] ?? [], TRUE);

      $storable_prop_shape = $prop_shape->getStorage();
      if (is_null($storable_prop_shape)) {
        continue;
      }
      $static_prop_source = $storable_prop_shape->toStaticPropSource();

      // @see `type: experience_builder.component.*`
      assert(array_key_exists('properties', $component_plugin->metadata->schema));
      $props[$cpe->propName] = [
        'field_type' => $storable_prop_shape->fieldTypeProp->fieldType,
        'field_widget' => $storable_prop_shape->fieldWidget,
        'expression' => (string) $storable_prop_shape->fieldTypeProp,
        // TRICKY: need to transform to the array structure that depends on the
        // field type. Do not store a default value for field types that
        // reference entities, because that would require those entities to be
        // created.
        // @see `type: field.storage_settings.*`
        'default_value' => self::exampleValueRequiresEntity($storable_prop_shape) ? [] : $static_prop_source->withValue(
          $is_required
            // Example guaranteed to exist if a required prop.
            ? $component_plugin->metadata->schema['properties'][$cpe->propName]['examples'][0]
            // Example may exist if an optional prop.
            : (
          array_key_exists('examples', $component_plugin->metadata->schema['properties'][$cpe->propName]) && array_key_exists(0, $component_plugin->metadata->schema['properties'][$cpe->propName]['examples'])
            ? $component_plugin->metadata->schema['properties'][$cpe->propName]['examples'][0]
            : NULL
          )
        )->fieldItem->getValue(),
        'field_storage_settings' => $storable_prop_shape->fieldStorageSettings ?? [],
        'field_instance_settings' => $storable_prop_shape->fieldInstanceSettings ?? [],
      ];
    }

    return $props;
  }

  /**
   * Whether this storable prop shape needs a (referenceable) entity created.
   *
   * TRICKY: SDCs whose storable prop shape uses an entity reference CAN NOT
   * ever have a default value specified in their corresponding Component config
   * entity.
   *
   * It is in fact possible to transform the example value in the SDC into a
   * corresponding real (saved) entity in Drupal, but that would pollute the
   * data stored in Drupal (the nodes, the media, …) with what would be
   * perceived as a nonsensical value.
   *
   * To avoid this pollution, we allow such SDC props to not specify a default
   * value for its StorablePropShape stored in the Component config entity.
   * To offer an equivalently smooth experience, with the specified example
   * value, XB instead is able to generate valid values for rendering a preview
   * of the SDC.
   *
   * Typical examples:
   * - an SDC prop accepting an image, i.e.
   *   `json-schema-definitions://experience_builder.module/image`. But other
   * - an SDC prop accepting a URL for a link, i.e.
   *   `type: string, format: uri-reference`
   *
   * Currently, this has only been necessary for URLs. More may be needed in the
   * future.
   *
   * @see \Drupal\experience_builder\PropSource\UrlPreviewPropSource
   * @see \Drupal\experience_builder\ComponentSource\UrlRewriteInterface
   */
  public static function exampleValueRequiresEntity(StorablePropShape $storable_prop_shape): bool {
    if ($storable_prop_shape->fieldTypeProp instanceof FieldTypeObjectPropsExpression) {
      if ($storable_prop_shape->fieldTypeProp->fieldType === 'entity_reference') {
        return TRUE;
      }
      else {
        foreach ($storable_prop_shape->fieldTypeProp->objectPropsToFieldTypeProps as $field_type_prop) {
          if ($field_type_prop instanceof ReferenceFieldTypePropExpression) {
            return TRUE;
          }
        }
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function clientModelToInput(string $component_instance_uuid, ComponentEntity $component, array $client_model, ?ConstraintViolationListInterface $violations = NULL): array {
    $props = [];

    foreach (($client_model['source'] ?? []) as $prop => $prop_source) {
      if ($violations === NULL && !\array_key_exists($prop, $client_model['resolved'])) {
        // Valueless prop, for the case where only a default is provided for the
        // preview, not for storing.
        // @see ::exampleValueRequiresEntity()
        // @see ::getClientSideInfo()
        $client_side_info = $this->getClientSideInfo($component);
        if (!isset($client_side_info['build']['#props'][$prop])) {
          // If the prop is not set at all, then no default is provided.
          continue;
        }
        assert(isset($client_side_info['field_data'][$prop]['jsonSchema']));
        $source = new UrlPreviewPropSource(
          value: $client_side_info['build']['#props'][$prop],
          jsonSchema: $client_side_info['field_data'][$prop]['jsonSchema'],
          componentId: $component->id(),
        );
        $props[$prop] = $source->toArray();
        continue;
      }

      $prop_value = $client_model['resolved'][$prop] ?? NULL;
      try {
        // @see PropSourceComponent type-script definition.
        // @see EvaluatedComponentModel type-script definition.
        // @todo We may not universally do this in the future to allow for
        // transformed values, e.g. for an image field the source value
        // might be a target_id whilst the resolved value might be the
        // image's alt, src, height and width properties. This will change
        // in https://drupal.org/i/3493942.
        // Undo what ::inputToClientModel() did: restore the omitted `'value'`.
        $prop_source['value'] = $prop_value;
        $source = PropSource::parse($prop_source);
      }
      catch (\LogicException) {
        // Invalid client data has been passed - fallback to an empty default
        // static prop source.
        $source = $this->getDefaultStaticPropSource($prop)->withValue([]);
      }
      if ($source instanceof StaticPropSource && $source->fieldItem instanceof EntityReferenceItemInterface &&
        // @todo Remove in https://www.drupal.org/i/3501902
        //   and/or https://www.drupal.org/i/3493943
        \is_array($prop_value)) {
        $target_type = $source->fieldItem->getFieldDefinition()
          ->getSetting('target_type');
        try {
          $target_id = $this->findTargetForProps($prop_value, $target_type);
        }
        catch (InvalidRequestBodyValue $invalid) {
          if ($violations !== NULL) {
            $violations->add(new ConstraintViolation(
              $invalid->getMessage(),
              NULL,
              [],
              $client_model,
              $invalid->propertyPath
                ? "model.$component_instance_uuid.$prop.{$invalid->propertyPath}"
                : "model.$component_instance_uuid.$prop",
              $prop_value,
            ));
          }
          continue;
        }

        $source = $source->withValue(
          array_diff_key($source->getValue(), \array_flip(['src', 'target_id']))
          + ['target_id' => $target_id]
        );
      }
      $props[$prop] = $source->toArray();
    }

    return $props;
  }

  /**
   * @todo This now is only for default values as the client now sends the
   *  target ID - Remove in https://www.drupal.org/i/3501902 and/or
   *  https://www.drupal.org/i/3493943
   */
  private function findTargetForProps(array $prop_value, string $target_type): int {
    if ($target_type !== 'media' && $target_type !== 'file') {
      // Once the 'target_id' is saved the target type won't be needed.
      throw new InvalidRequestBodyValue("Unsupported target type '$target_type'.");
    }
    $src = $prop_value['src'];

    // Only consider public files until we save 'target_id' in the client model.
    $base_path = '/' . PublicStream::basePath() . '/';
    $relative_path = substr($src, strlen($base_path));
    $drupal_uris = ['public://' . $relative_path];

    // This might be an image style from the adapted image input, in which
    // case the image will be in the format `files/styles/{style}/{url}`.
    if (str_contains($src, 'files/styles/thumbnail') && preg_match('@/files/styles/thumbnail/public/(.*).webp@', $src, $matches)) {
      $drupal_uris[] = 'public://' . $matches[1];
    }
    if (preg_match('@/sites/.*/files/(.*)$@', $src, $matches)) {
      // This could also be running in a sub-directory, for example in CI.
      // Let's just match on sites/default/files or
      // sites/simpletest/{testid}/files.
      $drupal_uris[] = 'public://' . $matches[1];
    }

    // Load the file entity using the 'uri'. 'filename' will not always work
    // because the file name can be changed in the uri.
    $files = $this->entityTypeManager->getStorage('file')->loadByProperties(['uri' => $drupal_uris]);
    $file = reset($files);
    if (!$file) {
      throw new InvalidRequestBodyValue("File '$src' not found.", 'src');
    }
    $file_id = $file->id();
    if ($target_type === 'file') {
      return (int) $file_id;
    }

    // TRICKY: this is tightly coupled to `media_library_storage_prop_shape_alter()`!
    $image_media_type = MediaType::load('image');
    assert($image_media_type !== NULL);
    $source_field_definition = $image_media_type->getSource()->getSourceFieldDefinition($image_media_type);
    assert($source_field_definition !== NULL);
    $source_field_name = $source_field_definition->getName();
    $query = $this->entityTypeManager->getStorage('media')->getQuery()->condition("$source_field_name.target_id", $file_id)->accessCheck();
    $media_ids = $query->execute();
    assert(is_array($media_ids));
    if (empty($media_ids)) {
      throw new InvalidRequestBodyValue("No media entity found that uses file '$src'.", 'src');
    }
    return (int) array_pop($media_ids);
  }

}
