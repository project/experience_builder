<?php

declare(strict_types=1);

namespace Drupal\experience_builder\PropExpressions\Component;

use Drupal\experience_builder\PropExpressions\PropExpressionInterface;

interface ComponentPropExpressionInterface extends PropExpressionInterface {

  // Components are for graphical representations.
  const PREFIX = '⿲';

}
