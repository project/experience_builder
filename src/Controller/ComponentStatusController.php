<?php

namespace Drupal\experience_builder\Controller;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\experience_builder\ComponentDoesNotMeetRequirementsException;
use Drupal\experience_builder\ComponentIncompatibilityReasonRepository;
use Drupal\experience_builder\Entity\Component;
use Drupal\experience_builder\Plugin\ComponentPluginManager;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Not every unavailable/disabled SDC will have Component entity, so we're using a controller instead of EntityListBuilder for this.
 *
 * @see \Drupal\experience_builder\Plugin\ComponentPluginManager::setCachedDefinitions()
 *
 * @todo Ensure reasons are translated.
 */
final class ComponentStatusController {

  use StringTranslationTrait;

  /**
   * @param \Drupal\experience_builder\Plugin\ComponentPluginManager $componentPluginManager
   */
  public function __construct(
    private readonly ComponentPluginManager $componentPluginManager,
    private readonly ComponentIncompatibilityReasonRepository $reasonRepository,
    private readonly MessengerInterface $messenger,
  ) {}

  public function __invoke(): array {
    // @todo State API is not guaranteed to stay in sync with SDC discovery cache and we should revisit this and choose more reliable, but still performant storage.
    // @see https://www.drupal.org/node/3177901
    $this->componentPluginManager->clearCachedDefinitions();
    $this->componentPluginManager->getDefinitions();

    $reasons = $this->reasonRepository->getReasons();
    $rows = [];
    $header = [
      [
        'data' => $this->t('Component'),
      ],
      [
        'data' => $this->t('Status'),
      ],
      [
        'data' => $this->t('Reason'),
      ],
    ];
    foreach ($reasons as $source_reasons) {
      foreach ($source_reasons as $component_id => $reason) {
        $component_entity = Component::load($component_id);
        $status = $component_entity instanceof Component && !$component_entity->status() ? $this->t('Disabled') : $this->t('Incompatible');

        $rows[] = [
          'data' => [
            $component_id,
            $status,
            Markup::create($reason),
          ],
        ];
      }
    }

    return [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No incompatible components detected.'),
    ];
  }

  /**
   * Calls a method on a component and reloads the listing page.
   *
   * @param \Drupal\experience_builder\Entity\Component $component
   *   The component being acted upon.
   * @param string $op
   *   The operation to perform, e.g., 'enable' or 'disable'.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect back to the listing page.
   */
  public function performOperation(Component $component, string $op) {
    assert(in_array($op, ['enable', 'disable']));

    $component_id = $component->id();
    $source = $component->getComponentSource();
    $source_plugin_id = $source->getPluginId();
    if ($op === 'disable') {
      $component->disable()->save();
      $this->reasonRepository->storeReason($source_plugin_id, $component_id, 'Manually disabled');
    }
    elseif ($op === 'enable') {
      try {
        $source->checkRequirements();
        $component->enable()->save();
        $this->reasonRepository->removeReason($source_plugin_id, $component_id);
      }
      catch (ComponentDoesNotMeetRequirementsException $e) {
        $this->messenger->addError($this->t('The component %component does not meet requirements: %reason', [
          "%component" => $component_id,
          "%reason" => $e->getMessage(),
        ]));
        $this->reasonRepository->storeReason($source_plugin_id, $component_id, $e->getMessage());
        return new RedirectResponse(Url::fromRoute('entity.component.collection')->toString());
      }
    }

    $this->messenger->addStatus($this->t('The component %component has been updated', [
      "%component" => $component_id,
    ]));
    return new RedirectResponse(Url::fromRoute('entity.component.collection')->toString());
  }

}
