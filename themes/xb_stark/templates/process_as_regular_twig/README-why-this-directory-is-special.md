Unless a template is inside a `process_as_jsx` directory, it is rendered as Twig. This `process_as_regular_twig` directory is not needed functionally, but it's named this way to make the purpose of `process_as_jsx` a little more clear.

