# Experience Builder

The Drupal Experience Builder module will 1) enable site builders without Drupal
experience to easily theme and build their entire website using only their
browser, without the need to write code beyond basic HTML, CSS, and templating
markup (e.g., Twig) AND 2) it will enable content creators to and compose
content on any part of the page without relying on developers.

## Requirements

This module requires the core Media and Media Library modules to support images.

## Installation

See the [CONTRIBUTING.md](CONTRIBUTING.md) steps to install.

## Configuration

See the [CONTRIBUTING.md](CONTRIBUTING.md) steps for configuration.

Optionally, you can install the [Starshot Demo Design System](https://www.drupal.org/community-initiatives/starshot-demo-design-system/sdds-development-guide)
which provides additional components for testing.
