# Experience Builder Components

In the rest of this document, `Experience Builder` will be written as `XB`.

**Also see the [diagram](diagrams/data-model.md).**

## 1. Terminology

### 1.1 Existing Drupal Terminology that is crucial for XB

- `SDC`: a [Single-Directory Component](https://www.drupal.org/project/sdc)
- `Block`: a [block plugin](https://www.drupal.org/docs/drupal-apis/block-api/block-api-overview) — ⚠️ not to be confused with `Block` config entities

### 1.2 XB terminology

- `component`: a component generates markup (and might attach CSS + JS), potentially based on some input. ⚠️ This is currently limited to `SDC`s, but that _will_ change. So: read this more broadly. ⚠️
- `Component Source Plugin`: `component`s have a translation layer (per `component type`) between the `Component` config entity and the actual plugin that
  accepts input and generates output, e.g. `SingleDirectoryComponent` (`sdc`-prefixed) and `BlockComponent` (`block`-prefixed).
- `component input`: either `explicit component input` or `implicit component input`, this is the catch-all for both
- `component slot`: each component may in its metadata define 0 or more slots, each slot accepts >=0 component instances in a particular order
- `component type`: the ID of a `Component Source Plugin` — currently either "sdc", "block" or "js"
- `explicit component input`: each `component` may in its metadata define 0 or more explicit inputs to be provided by the person placing an instance of this `component`, each input accepts structured data conforming to the shape defined in the `component`'s (`ComponentSourcePlugin`-specific) metadata
- `implicit component input`: in contrast with a `explicit component input`, an implicit input is not provided by the person placing an instance: it is implicitly present: it is provided by other content displayed on the current route, by the request context (e.g. URL query string, current user …) or by the environment (e.g. time of day).
  - ⚠️This is not yet supported. the `Block` component type needs it to support [_contexts_](https://www.drupal.org/docs/drupal-apis/plugin-api/plugin-contexts#s-context-on-blocks), unclear how that will be surfaced in XB; initially, only block plugins that do not require contexts are supported.
- `block override`: a JavaScript component may be used to override the output of an existing Drupal `Block` plugin.
  - ⚠️This feature is highly experimental and subject to change.

## 2. Product requirements

This uses the terms defined above.

(There are [more](https://docs.google.com/spreadsheets/d/1OpETAzprh6DWjpTsZG55LWgldWV_D8jNe9AM73jNaZo/edit?gid=1721130122#gid=1721130122), but these in particular affect XB's supported components.)

- MUST be possible for the Site Builder to control, audit and synchronize which `component`s are available for Content Creators → see [`XB Config Management` doc](config-management.md)
- MUST support `SDC` and `Block` today
  - MUST be evolvable to [support other component types later](https://www.drupal.org/project/experience_builder/issues/3454519)
- MUST support existing `SDC`s and `Block`s, if they meet certain criteria necessary for XB to provide a good UX
- MUST support categorization of `component`s
- MAY require API additions and perhaps even changes to `SDC`s (such as: defining restrictions for `component slot`s, schema references and more) ⚠️ [an overview of what has been identified is constantly updated](https://www.drupal.org/project/experience_builder/issues/3462705) ⚠️

⚠️ The [supported component modeling approaches](https://www.drupal.org/project/experience_builder/issues/3446083)
are not yet finalized. That is likely to affect the requirements above.


## 3. Implementation

This uses the terms defined above.


### 3.1 `SDC` `component`s

#### 3.1.1 Inputs & instantiation UX for `SDC` `component`s
`SDC` `component`s specify the accepted explicit inputs ("props") and their shapes in a `*.component.yml` file. The
shapes (and  semantics!) are defined using JSON schema. Defaults are present as an `example`, which is a feature of JSON
schema.

`SDC` `component`s DO NOT accept implicit inputs.

`SDC` DOES NOT provide an input UX, so its `Component Source Plugin` must do so on its behalf; and does so by matching
available field types against the JSON schema of its explicit inputs ("props"). For details, see the [`XB Shape Matching
into Field Types` doc](shape-matching-into-field-types.md).

#### 3.1.2 Criteria for `SDC` `component`s

For an `SDC` to be compatible/eligible for use in XB, it:
- MUST always have schema, even for theme `SDC`s
- MUST have `title` for each prop
- MUST have `example` for each required prop
- MUST have only props for whose `prop shape`s a `static prop source` can be found (see the [`XB Shape Matching into Field
  Types` doc, section 3.1.2.b](shape-matching-into-field-types.md#3.1.2.b).
- MUST not have `status` value `obsolete`
- SHOULD have a `category`; if not specified, the fallback value "Other" will be used

These checks are implemented in `\Drupal\experience_builder\ComponentMetadataRequirementsChecker`.

_Note:_ this list of criteria is not final, it will keep evolving _at least_ until a `1.0` release of XB.

#### 3.1.2 Missing `SDC` functionality that XB already implements ahead of availability in Drupal core
- Schema references support (stored in per-extension `/schema.json` files), upstream issue: [#3352063](https://www.drupal.org/project/drupal/issues/3352063)
- … likely more to come, see the [full list](https://www.drupal.org/project/experience_builder/issues/3462705)


### 3.2 `Block` `component`s

An immediate question that will come to mind when reading this: why `Block` the _plugins_ and not `Block` the _config entities_?
It does not make sense to surface the config entities, because:
1. they're hard-coupled to a theme (region): they're a "placed block" in the Drupal UI!
2. there can be multiple instances ("placements") of the same block plugin, each with a different label, but they'd render exactly the same in XB

Therefore, it only makes sense to surface _block plugins_ as XB `component`s.

#### 3.2.1 Inputs & instantiation UX for `Block` `component`s

`Block` `component`s specify the accepted explicit inputs and their shapes in `*.schema.yml` file. The shapes are
defined  using config schema (`type: block.settings.<PLUGIN ID>`). Defaults are present as the
`::defaultConfiguration()` method  on the PHP plugin class.

`Block` `component`s DO accept implicit inputs, in two ways even:
1. Logic in the block plugin can fetch data — through database queries, HTTP requests, anything.
2. Contexts. Not yet supported. (⚠️ handling contexts is still TBD in [#3485502](https://www.drupal.org/project/experience_builder/issues/3485502))

`Block` `component`s specify the accepted explicit inputs. They typically allow influencing the logic in the block
plugin. These explicit inputs can hence be seen as knobs and levers to adjust what the underlying block plugin does.

`Block` DOES provide an input UX (`BlockPluginInterface::buildConfigurationForm()`), so its `Component Source Plugin`
simply reuses that.

#### 3.2.2 Criteria for `Block` `component`s

For a `Block` to be compatible/eligible for use in XB it:
 - MUST have fully validatable block plugin settings config schema via the `FullyValidatable` constraint
 - MUST NOT have any required context (⚠️ handling contexts is still TBD in [#3485502](https://www.drupal.org/project/experience_builder/issues/3485502))

These checks are implemented in `experience_builder_block_alter()`.

_Note:_ this list of criteria is not final, it will keep evolving _at least_ until a `1.0` release of XB.

#### 3.2.2 Special consideration: migration from Layout Builder

The Drupal core Layout Builder module is block-centric. A migration from Layout Builder to Experience Builder (and ideally: an _upgrade_ path) MUST remain possible.

(This is product requirement [`39. Layout Builder Migration`](https://docs.google.com/spreadsheets/d/1OpETAzprh6DWjpTsZG55LWgldWV_D8jNe9AM73jNaZo/edit?gid=1721130122#gid=1721130122&range=B51).)

Layout Builder's data model is centered around 1) layout plugins, 2) blocks. Layout plugins are used to arrange instances of blocks in a particular layout. But while all documentation and UI pieces
refer to "blocks" and not "block plugins", under the hood, they actually _are_ block plugins! See `type: layout_builder.component` in `layout_builder.schema.yml`. Many details are still to be figured out for that, but that is for later.



### 3.3 `JS` `component`s (aka "code components")

#### 3.3.1 Inputs & instantiation UX for `JS` `component`s

`JS` `component`s specify the accepted explicit inputs ("props") and their shapes in the underlying
`JavaScriptComponent config entity`, using the exact same format (JSON schema) as `SDC` `component`s (see [section
3.1.1 above](#3.1.1)). See [section 3.2 `JavaScriptComponent config entity` in the `XB Config Management`
doc](config-management.md#3.2) for all details.

`JS` `component`s DO NOT accept implicit inputs.

`JS` DOES NOT provide an input UX, so its `Component Source Plugin` must do so on its behalf; and does so by matching
available field types against the JSON schema of its explicit inputs ("props"). For details, see the [`XB Shape Matching
into Field Types` doc](shape-matching-into-field-types.md). (It shares this infrastructure with the `SDC` `Component
Source Plugin`.)

#### 3.3.2 Criteria for `JS` `component`s

There aren't any, because the validation logic for the `JavaScriptComponent config entity` type prevents saving a `JS`
`component` whose inputs XB cannot generate an input UX for: it imposes the same criteria as for `SDC` `component`s (see
[section 3.1.2 above](#3.1.2)).

#### 3.3.3 Block overrides ⚠️ warning: experimental!

The `JavaScriptComponent config entity` has a special "block_override" property that, when set, enables the `component`
declared in the entity to take over the rendering of the specified `Block` plugin ID.

Mapping of block content into props and slots is done by specially named preprocess functions. The existing block plugin
preprocess function is suffixed with "__as_js_component" and this is called after the other existing preprocess functions.

The data output by each block plugin must currently be mapped on an individual basis. This feature is highly experimental
and subject to change.

### 3.4 Other `component type`s

Nothing yet, this will change when we [support other `component type`s later](https://www.drupal.org/project/experience_builder/issues/3454519).

### 3.5 Categorization

Each `component` can be categorized in order to group them in the UI. Some `component type`s have shared categories, as follows:

```mermaid
stateDiagram-v2
classdef source color:white,fill:purple

state "JavaScript component" as JavaScript
state "Theme component" as Theme
state "Theme component categories" as ThemeCat
state "Module component" as Module
state "Module component categories" as ModuleCat
state "Element categories" as ElementCat
state "Block categories" as BlockCat

JavaScript:::source --> ThemeCat
Theme:::source --> ThemeCat
Module:::source --> ModuleCat
Element:::source --> ElementCat
Block:::source --> BlockCat
```
