<?php

declare(strict_types=1);

namespace Drupal\Tests\experience_builder\Functional;

use Drupal\Component\Uuid\Uuid;
use Drupal\Core\Url;
use Drupal\experience_builder\Entity\PageRegion;
use Drupal\experience_builder\Plugin\DataType\ComponentTreeStructure;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\experience_builder\Traits\GenerateComponentConfigTrait;

/**
 * @group experience_builder
 * @covers \experience_builder_form_system_theme_settings_alter()
 * @covers \experience_builder_form_system_theme_settings_submit()
 * @covers \Drupal\experience_builder\Entity\PageRegion::createFromBlockLayout()
 */
class XbPageVariantEnableTest extends BrowserTestBase {

  use GenerateComponentConfigTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['block', 'experience_builder', 'node'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'olivero';

  public function test(): void {
    $this->drupalLogin($this->rootUser);
    $this->generateComponentConfig();

    // No XB settings on the global settings page.
    $this->drupalGet('/admin/appearance/settings');
    $this->assertSession()->pageTextNotContains('Experience Builder');
    $this->assertSession()->fieldNotExists('use_xb');

    // XB checkbox on the Olivero theme page.
    $this->drupalGet('/admin/appearance/settings/olivero');
    $this->assertSession()->pageTextContains('Experience Builder');
    $this->assertSession()->fieldExists('use_xb');

    // We start with no templates.
    $this->assertEmpty(PageRegion::loadMultiple());

    // No template is created if we do not enable XB.
    $this->submitForm(['use_xb' => FALSE], 'Save configuration');
    $this->assertEmpty(PageRegion::loadMultiple());

    // Regions are created when we enable XB.
    $this->submitForm(['use_xb' => TRUE], 'Save configuration');
    $regions = PageRegion::loadMultiple();
    $this->assertCount(12, $regions);

    // Check the regions are created correctly.
    $expected_page_region_ids = \array_filter([
      'olivero.breadcrumb',
      'olivero.content_above',
      'olivero.content_below',
      'olivero.footer_bottom',
      'olivero.footer_top',
      'olivero.header',
      'olivero.hero',
      'olivero.highlighted',
      'olivero.primary_menu',
      'olivero.secondary_menu',
      'olivero.sidebar',
      'olivero.social',
    ]);
    $regions_with_component_tree = [];
    foreach ($regions as $region) {
      if ($data = $region->getComponentTree()->toArray()) {
        $regions_with_component_tree[$region->id()] = $data;
      }
    }
    $this->assertSame(\array_values($expected_page_region_ids), array_keys($regions_with_component_tree));

    foreach ($regions_with_component_tree as $tree) {
      $this->assertJson($tree['tree']);
      $tree = json_decode($tree['tree'], TRUE);
      $this->assertArrayHasKey(ComponentTreeStructure::ROOT_UUID, $tree);
      foreach ($tree[ComponentTreeStructure::ROOT_UUID] as $component) {
        $this->assertTrue(Uuid::isValid($component['uuid']));
        $this->assertStringStartsWith('block.', $component['component']);
      }
    }
    $front = Url::fromRoute('<front>');
    $this->drupalGet($front);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementsCount('css', '#primary-tabs-title', 1);

    // The template is disabled again when we disable XB.
    $this->drupalGet('/admin/appearance/settings/olivero');
    $this->submitForm(['use_xb' => FALSE], 'Save configuration');
    $regions = PageRegion::loadMultiple();
    $this->assertCount(12, $regions);
    foreach ($regions as $region) {
      $this->assertFalse($region->status());
    }

    $this->drupalGet($front);
    $this->assertSession()->statusCodeEquals(200);
  }

}
