<?php

declare(strict_types=1);

namespace Drupal\Tests\experience_builder\Kernel\Config;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\experience_builder\ComponentIncompatibilityReasonRepository;
use Drupal\experience_builder\Entity\Component;
use Drupal\experience_builder\Entity\ComponentInterface;
use Drupal\experience_builder\Entity\JavaScriptComponent;
use Drupal\experience_builder\Plugin\ExperienceBuilder\ComponentSource\JsComponent;
use Drupal\Tests\experience_builder\Traits\ConstraintViolationsTestTrait;
use Drupal\Tests\experience_builder\Traits\GenerateComponentConfigTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Tests JavascriptComponentStorage.
 *
 * @covers \Drupal\experience_builder\EntityHandlers\JavascriptComponentStorage
 * @covers \Drupal\experience_builder\Plugin\ExperienceBuilder\ComponentSource\JsComponent::createConfigEntity
 * @covers \Drupal\experience_builder\Plugin\ExperienceBuilder\ComponentSource\JsComponent::updateConfigEntity
 * @group JavaScriptComponents
 * @group experience_builder
 */
final class JavascriptComponentStorageTest extends AssetLibraryStorageTest {

  use UserCreationTrait;
  use ConstraintViolationsTestTrait;
  use GenerateComponentConfigTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'experience_builder',
    'user',
    'system',
    // XB's dependencies (modules providing field types + widgets).
    'datetime',
    'file',
    'image',
    'options',
    'path',
    'link',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('user');
    $this->installConfig(['system']);
  }

  /**
   * @covers \Drupal\experience_builder\EntityHandlers\XbAssetStorage::generateFiles()
   */
  public function testGeneratedFiles(): void {
    $js_component = JavaScriptComponent::create([
      'machineName' => $this->randomMachineName(),
      'name' => $this->getRandomGenerator()->sentences(5),
      'status' => FALSE,
      'props' => [],
      'required' => [],
      'slots' => [],
      'js' => [
        'original' => 'console.log("hey");',
        'compiled' => 'console.log("hey");',
      ],
      'css' => [
        'original' => '.test { display: none; }',
        'compiled' => '.test { display: none; }',
      ],
    ]);
    $this->assertGeneratedFiles($js_component);
  }

  /**
   * @covers \Drupal\experience_builder\Entity\JavaScriptComponent::$block_override
   * @covers \Drupal\experience_builder\Plugin\ExperienceBuilder\ComponentSource\BlockComponent::renderComponent()
   */
  public function testBlockOverrideActivation(): void {
    // Initial situation: block module is installed, Components are created for
    // those blocks.
    $this->enableModules(['block']);
    $this->generateComponentConfig();
    $this->assertInstanceOf(Component::class, Component::load('block.system_menu_block.main'));
    $this->assertInstanceOf(Component::class, Component::load('block.system_menu_block.tools'));

    // Simulate those block components having been instantiated, rendered, and
    // live in render cache, Dynamic Page Cache and Page Cache.
    // @see \Drupal\experience_builder\Plugin\ExperienceBuilder\ComponentSource\BlockComponent::renderComponent()
    $cache = \Drupal::cache();
    $cache->set('✋', $this->randomString(), tags: Component::load('block.system_menu_block.main')->getCacheTags());
    $cache->set('🛠️', $this->randomString(), tags: Component::load('block.system_menu_block.tools')->getCacheTags());
    $this->assertNotNull($cache->get('✋'));
    $this->assertNotNull($cache->get('🛠️'));

    // Now create a JavaScript Component.
    $js_component = JavaScriptComponent::create([
      'machineName' => $this->randomMachineName(),
      'name' => $this->getRandomGenerator()->sentences(5),
      'status' => FALSE,
      'props' => [],
      'required' => [],
      'slots' => [],
      'js' => [
        'original' => 'console.log("hey");',
        'compiled' => 'console.log("hey");',
      ],
      'css' => [
        'original' => '.test { display: none; }',
        'compiled' => '.test { display: none; }',
      ],
    ]);
    $js_component->save();

    // It is valid, and render cached block component instances still exist.
    self::assertEmpty($js_component->getTypedData()->validate());
    $this->assertNotNull($cache->get('✋'));
    $this->assertNotNull($cache->get('🛠️'));

    // Specify unsupported block override: error.
    $js_component->set('block_override', 'local_tasks_block');
    self::assertSame('The value you selected is not a valid choice.', (string) $js_component->getTypedData()->validate()->get(0)->getMessage());

    // Specify supported block override: valid.
    $js_component->set('block_override', 'system_menu_block');
    self::assertEmpty($js_component->getTypedData()->validate());
    $js_component->save();
    // Render cached block component instances still exist, because the code
    // component is "internal".
    $this->assertNotNull($cache->get('✋'));
    $this->assertNotNull($cache->get('🛠️'));

    // Now change the code component from "internal" to "exposed".
    $js_component->enable()->save();
    // Render cached block component instances NO LONGER exist.
    $this->assertFalse($cache->get('✋'));
    $this->assertFalse($cache->get('🛠️'));

    // This proves that the next time BlockComponent::renderComponent() runs, it
    // will use the overriding JavaScriptComponent config entity instead.
  }

  /**
   * @covers \Drupal\experience_builder\Plugin\ExperienceBuilder\ComponentSource\JsComponent::createConfigEntity()
   */
  public function testComponentEntityCreation(): array {
    $js_component_id = $this->randomMachineName();
    $component_id = JsComponent::componentIdFromJavascriptComponentId($js_component_id);
    $reason_repository = $this->container->get(ComponentIncompatibilityReasonRepository::class);

    // When the JS component does not exist, nor should the component config
    // entity.
    $component = Component::load($component_id);
    self::assertNull($component);

    // Now let's create the JavaScript component.
    // Should fail - missing examples.
    $props = [
      'title' => [
        'type' => 'string',
        'title' => 'Title',
      ],
    ];
    $js_component = JavaScriptComponent::create([
      'machineName' => $js_component_id,
      'name' => $this->getRandomGenerator()->sentences(5),
      'status' => FALSE,
      'props' => $props,
      'required' => ['title'],
      'slots' => [],
      'js' => [
        'original' => 'console.log("hey");',
        'compiled' => 'console.log("hey");',
      ],
      'css' => [
        'original' => '.test { display: none; }',
        'compiled' => '.test { display: none; }',
      ],
    ]);
    $this->assertSame([
      '' => 'Prop "title" is required, but does not have example value',
    ], self::violationsToArray($js_component->getTypedData()->validate()));

    // Make it pass validation by adding the missing `examples`, and save it.
    $props['title']['examples'] = ['Title'];
    $js_component->setProps($props);
    $this->assertSame([], self::violationsToArray($js_component->getTypedData()->validate()));
    $js_component->save();

    // No Component config entity is ever created for JavaScript Components not
    // explicitly flagged to be added to XB's component library.
    $component = Component::load($component_id);
    self::assertEmpty($reason_repository->getReasons()[JsComponent::SOURCE_PLUGIN_ID] ?? []);
    self::assertNull($component);

    // Use a non-storable prop shape. The JavaScript Component config entity's
    // config schema SHOULD prevent the component author from choosing props
    // that the Experience Builder cannot generate an input UX for.
    // @see \Drupal\experience_builder\Plugin\ExperienceBuilder\ComponentSource\GeneratedFieldExplicitInputUxComponentSourceBase
    // @see the `Choice` constraints on `type: experience_builder.js_component.*`'s for prop `format`.
    $props['title']['format'] = 'hostname';
    $js_component->setProps($props);
    $this->assertSame([
      '' => 'Experience Builder does not know of a field type/widget to allow populating the <code>title</code> prop, with the shape <code>{"type":"string","format":"hostname"}</code>.',
      'props.title.format' => 'The value you selected is not a valid choice.',
    ], self::violationsToArray($js_component->getTypedData()->validate()));
    // @see the `Choice` constraints on `type: experience_builder.js_component.*`'s for prop `type`.
    unset($props['title']['format']);
    $props['title']['type'] = 'array';
    $js_component->setProps($props);
    $this->assertSame([
      '' => 'Experience Builder does not know of a field type/widget to allow populating the <code>title</code> prop, with the shape <code>{"type":"array"}</code>.',
      'props.title.type' => 'The value you selected is not a valid choice.',
    ], self::violationsToArray($js_component->getTypedData()->validate()));

    // In other words: if the JavaScript Component config entity is sufficiently
    // tightly validated, the following should always be true.
    self::assertSame([], $reason_repository->getReasons()[JsComponent::SOURCE_PLUGIN_ID] ?? []);

    // Now remove the attempts to bypass the JavaScriptComponent config entity's
    // validation, enable it and verify that a corresponding Component config
    // entity is created.
    $props['title']['type'] = 'string';
    $js_component
      ->setProps($props)
      ->enable()
      ->save();

    $component = Component::load($component_id);
    self::assertInstanceOf(ComponentInterface::class, $component);
    self::assertNull($component->get('provider'));
    self::assertEquals(['title'], \array_keys($component->getSettings()['prop_field_definitions']));

    // Now update the js component and confirm we update the matching component.
    $props['noodles'] = [
      'type' => 'string',
      'title' => 'What sort of noodles do you like?',
      'examples' => ['Soba', 'Wheat', 'Pool'],
    ];
    $new_name = 'Will you accept my name?';
    $js_component->set('name', $new_name);
    $js_component->setProps($props)->save();

    $component = $this->loadComponent($component_id);
    self::assertEquals($new_name, $component->label());
    self::assertEquals(['noodles', 'title'], \array_keys($component->getSettings()['prop_field_definitions']));

    return $js_component->toArray();
  }

  /**
   * @covers \Drupal\experience_builder\Plugin\ExperienceBuilder\ComponentSource\JsComponent::updateConfigEntity()
   * @depends testComponentEntityCreation
   */
  public function testComponentEntityUpdate(array $js_component_values): void {
    $js_component = JavaScriptComponent::create($js_component_values);
    $js_component->save();
    assert(is_string($js_component->id()));
    $component_id = JsComponent::componentIdFromJavascriptComponentId($js_component->id());

    // Name should carry over.
    $new_name = $js_component->label() . ' — updated';
    $js_component->set('name', $new_name)->save();
    $this->assertSame($new_name, $this->loadComponent($component_id)->label());

    // Status should carry over.
    $this->assertTrue($js_component->status());
    $this->assertTrue($this->loadComponent($component_id)->status());
    $js_component->disable()->save();
    $this->assertFalse($js_component->status());
    $this->assertFalse($this->loadComponent($component_id)->status());
    $js_component->enable()->save();
    $this->assertTrue($js_component->status());
    $this->assertTrue($this->loadComponent($component_id)->status());
  }

  private function loadComponent(string $id): Component {
    // @phpstan-ignore-next-line
    return $this->container->get(EntityTypeManagerInterface::class)
      ->getStorage(Component::ENTITY_TYPE_ID)
      ->loadUnchanged($id);
  }

}
