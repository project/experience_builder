<?php

declare(strict_types=1);

namespace Drupal\Tests\experience_builder\Kernel\Config;

// cspell:ignore sofie

use Drupal\experience_builder\Entity\JavaScriptComponent;
use Drupal\KernelTests\Core\Config\ConfigEntityValidationTestBase;

/**
 * Tests validation of JavaScriptComponent entities.
 *
 * @group experience_builder
 * @group JavaScriptComponents
 */
class JavaScriptComponentValidationTest extends ConfigEntityValidationTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'experience_builder',
    // XB's dependencies (the subset that is needed for these tests).
    'file',
    'image',
    'options',
  ];

  /**
   * {@inheritdoc}
   *
   * @phpstan-ignore property.defaultValue
   */
  protected static array $propertiesWithRequiredKeys = [
    'css' => [
      "'original' is a required key.",
      "'compiled' is a required key.",
    ],
    'js' => [
      "'original' is a required key.",
      "'compiled' is a required key.",
    ],
  ];

  /**
   * {@inheritdoc}
   */
  protected static array $propertiesWithOptionalValues = [
    'block_override',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->entity = JavaScriptComponent::create([
      'machineName' => 'test',
      'name' => 'Test',
      'status' => TRUE,
      'props' => [
        'text' => [
          'type' => 'string',
          'title' => 'Title',
          'examples' => ['Press', 'Submit now'],
        ],
      ],
      'slots' => [
        'test-slot' => [
          'title' => 'test',
          'description' => 'Title',
          'examples' => [
            'Test 1',
            'Test 2',
          ],
        ],
      ],
      'js' => [
        'original' => 'console.log("Test")',
        'compiled' => 'console.log("Test")',
      ],
      'css' => [
        'original' => '.test { display: none; }',
        'compiled' => '.test{display:none;}',
      ],
    ]);
    $this->entity->save();
  }

  /**
   * @testWith [true, true, []]
   *           [true, false, {"": "Prop \"silly\" is required, but does not have example value"}]
   *           [false, true, []]
   *           [false, false, []]
   */
  public function testPropExample(bool $required, bool $has_example, array $expected_validation_errors): void {
    $test_prop_definition = [
      'type' => 'boolean',
      'title' => $this->randomMachineName(),
      'examples' => [TRUE],
    ];
    if (!$has_example) {
      unset($test_prop_definition['examples']);
    }
    $this->entity
      ->set('required', $required ? ['silly'] : [])
      ->set('props', ['silly' => $test_prop_definition]);
    $this->assertValidationErrors($expected_validation_errors);
  }

  /**
   * @testWith ["string", ["the answer", "Wim", "Sofie", "Jack"], null]
   *           ["integer", [42, 1988, 1992, 2024], null]
   *           ["number", [3.14, 1.0], null]
   *           ["string", ["string", 42, 3.14], ["string", "42", "3.14"]]
   *           ["number", [42, 0], [42.0, 0.0]]
   */
  public function testValidEnumsAndExamples(string $json_schema_type, array $enum_and_examples_both, ?array $expected_typecasting): void {
    $this->entity->set('props', [
      'tested_enum_prop' => [
        'type' => $json_schema_type,
        'title' => "enum: $json_schema_type",
        'enum' => $enum_and_examples_both,
        'examples' => $enum_and_examples_both,
      ],
    ]);
    $this->assertValidationErrors([]);
    $this->entity->save();

    // The expected output (i.e. after saving) is the input. But in a few cases,
    // typecasting may occur. For readability, the third parameter is only
    // required for those cases.
    $expected = $expected_typecasting ?? $enum_and_examples_both;

    $this->assertSame($expected, $this->entity->get('props')['tested_enum_prop']['enum']);
    $this->assertSame($expected, $this->entity->get('props')['tested_enum_prop']['examples']);
  }

  /**
   * @testWith ["string", ["string", 42, 3.14, null], {"3": "This value should not be null."}]
   *           ["integer", ["string", 42, 3.14, null], {"0": "This value should be of the correct primitive type.", "2": "This value should be of the correct primitive type.", "3": "This value should not be null."}]
   *           ["number", ["string", 42, 3.14, null], {"0": "This value should be of the correct primitive type.", "3": "This value should not be null."}]
   */
  public function testInvalidEnumsAndExamples(string $json_schema_type, array $enum_and_examples_both, array $expected_validation_errors): void {
    $this->entity->set('props', [
      'tested_enum_prop' => [
        'type' => $json_schema_type,
        'title' => "enum: $json_schema_type",
        'enum' => $enum_and_examples_both,
        'examples' => $enum_and_examples_both,
      ],
    ]);

    // The expected validation errors are keyed by the index whose value in the
    // $enum_and_examples_both array is expected to trigger a validation error.
    // This is then expanded to expect an explicit validation error for that
    // same index in both `enum` and `examples`, hence ensuring consistent
    // validation for both.
    $expanded_expected_validation_errors = [];
    foreach ($expected_validation_errors as $index => $expected_validation_error) {
      $expanded_expected_validation_errors["props.tested_enum_prop.enum.$index"] = $expected_validation_error;
      $expanded_expected_validation_errors["props.tested_enum_prop.examples.$index"] = $expected_validation_error;
    }
    $this->assertValidationErrors($expanded_expected_validation_errors);
  }

  /**
   * Tests `type: boolean` validation and edge cases.
   *
   * (Cannot be tested generically, like `string`, `integer` and `number`.)
   */
  public function testBooleanPropDefinition(): void {
    // Try using `enum` on a boolean prop.
    $this->entity->set('props', [
      'some_boolean' => [
        'type' => 'boolean',
        'title' => 'either/or',
        'enum' => [TRUE, FALSE],
        'examples' => [TRUE, NULL, FALSE],
      ],
    ]);
    $this->assertValidationErrors([
      'props.some_boolean' => "'enum' is an unknown key because props.some_boolean.type is boolean (see config schema type experience_builder.json_schema.prop.boolean).",
      'props.some_boolean.examples.1' => 'This value should not be null.',
    ]);
  }

  /**
   * Tests `type: object` validation and edge cases.
   *
   * (Cannot be tested generically, like `string`, `integer` and `number`.)
   */
  public function testObjectPropDefinition(): void {
    $this->entity->set('props', [
      'some_object' => [
        'type' => 'object',
        '$ref' => 'json-schema-definitions://experience_builder.module/image',
        'title' => $this->randomString(),
        'enum' => [[], NULL],
        'examples' => [[], NULL],
      ],
    ]);
    $this->assertValidationErrors([
      'props.some_object.enum.0' => 'This value should not be blank.',
      'props.some_object.enum.1' => 'This value should not be null.',
      'props.some_object.examples.0' => 'This value should not be blank.',
      'props.some_object.examples.1' => 'This value should not be null.',
    ]);
  }

  /**
   * Tests different permutations of entity values.
   *
   * @param array $shape
   *   Array of entity values.
   * @param array $expected_errors
   *   Expected validation errors.
   *
   * @dataProvider providerTestEntityShapes
   */
  public function testEntityShapes(array $shape, array $expected_errors): void {
    $this->entity = JavaScriptComponent::create($shape);
    $this->assertValidationErrors($expected_errors);
  }

  public static function providerTestEntityShapes(): array {
    return [
      'Invalid: no JS' => [
        [
          'machineName' => 'test-no-slots-no-props',
          'name' => 'Test',
          'props' => [],
          'slots' => [],
          'js' => [
            'original' => NULL,
            'compiled' => NULL,
          ],
          'css' => [
            'original' => '.test { display: none; }',
            'compiled' => '.test{display:none;}',
          ],
        ],
        [
          'js.compiled' => 'This value should not be null.',
          'js.original' => 'This value should not be null.',
        ],
      ],
      'Invalid: Unknown prop type' => [
        [
          'machineName' => 'test-unknown-prop-type',
          'name' => 'Test',
          'props' => [
            'mixed_up_prop' => [
              'type' => 'unknown',
              'title' => 'Title',
              'enum' => [
                'Press',
                'Click',
                'Submit',
              ],
              'examples' => ['Press', 'Submit now'],
            ],
          ],
          'slots' => [],
          'js' => [
            'original' => 'console.log("Test")',
            'compiled' => 'console.log("Test")',
          ],
          'css' => [
            'original' => '.test { display: none; }',
            'compiled' => '.test{display:none;}',
          ],
        ],
        [
          '' => 'Unable to find class/interface "unknown" specified in the prop "mixed_up_prop" for the component "experience_builder:test-unknown-prop-type".',
          'props.mixed_up_prop' => "'enum' is an unknown key because props.mixed_up_prop.type is unknown (see config schema type experience_builder.json_schema.prop.*).",
          'props.mixed_up_prop.type' => 'The value you selected is not a valid choice.',
        ],
      ],
      'Valid: no props and no slots' => [
        [
          'machineName' => 'test-no-slots-no-props',
          'name' => 'Test',
          'props' => [],
          'slots' => [],
          'js' => [
            'original' => 'console.log("Test")',
            'compiled' => 'console.log("Test")',
          ],
          'css' => [
            'original' => '.test { display: none; }',
            'compiled' => '.test{display:none;}',
          ],
        ],
        [],
      ],
      'Valid: props (of all supported types), of which two required and no slots' => [
        [
          'machineName' => 'test-props-no-slots',
          'name' => 'Test',
          'props' => [
            'string' => [
              'type' => 'string',
              'title' => 'Title',
              'examples' => ['Press', 'Submit now'],
            ],
            'boolean' => [
              'type' => 'boolean',
              'title' => 'Truth',
              'examples' => [TRUE, FALSE],
            ],
            'integer' => [
              'type' => 'integer',
              'title' => 'Integer',
              'examples' => [23, 10, 2024],
            ],
            'number' => [
              'type' => 'number',
              'title' => 'Number',
              'examples' => [3.14],
            ],
          ],
          'required' => [
            'string',
            'integer',
          ],
          'slots' => [],
          'js' => [
            'original' => 'console.log("Test")',
            'compiled' => 'console.log("Test")',
          ],
          'css' => [
            'original' => '.test { display: none; }',
            'compiled' => '.test{display:none;}',
          ],
        ],
        [],
      ],
      'Invalid: a non-existent required prop' => [
        [
          'machineName' => 'test-non-existent-required-prop',
          'name' => 'Test',
          'props' => [
            'string' => [
              'type' => 'string',
              'title' => 'Title',
              'examples' => ['Press', 'Submit now'],
            ],
          ],
          'required' => [
            'does_not_exist',
          ],
          'slots' => [],
          'js' => [
            'original' => 'console.log("Test")',
            'compiled' => 'console.log("Test")',
          ],
          'css' => [
            'original' => '.test { display: none; }',
            'compiled' => '.test{display:none;}',
          ],
        ],
        [
          // ⚠️ SDC does not complain about this!
          // @see \Drupal\Core\Theme\Component\ComponentValidator
          // @todo Update once https://www.drupal.org/project/drupal/issues/3493086 is fixed.
        ],
      ],
      'Valid: props, no slots set' => [
        [
          'machineName' => 'test-props-no-slots',
          'name' => 'Test',
          'props' => [
            'text' => [
              'type' => 'string',
              'title' => 'Title',
              'examples' => ['Press', 'Submit now'],
            ],
          ],
          'js' => [
            'original' => 'console.log("Test")',
            'compiled' => 'console.log("Test")',
          ],
          'css' => [
            'original' => '.test { display: none; }',
            'compiled' => '.test{display:none;}',
          ],
        ],
        [],
      ],
      'Valid: enum props' => [
        [
          'machineName' => 'test-props-no-slots',
          'name' => 'Test',
          'props' => [
            'text' => [
              'type' => 'string',
              'title' => 'Title',
              'enum' => [
                'Press',
                'Click',
                'Submit',
              ],
              'examples' => ['Press', 'Submit now'],
            ],
          ],
          'slots' => [],
          'js' => [
            'original' => 'console.log("Test")',
            'compiled' => 'console.log("Test")',
          ],
          'css' => [
            'original' => '.test { display: none; }',
            'compiled' => '.test{display:none;}',
          ],
        ],
        [],
      ],
      'Valid: slots (one with description+examples, one without), no props' => [
        [
          'machineName' => 'test-slots',
          'status' => TRUE,
          'name' => 'Test',
          'props' => [],
          'slots' => [
            'test-slot' => [
              'title' => 'test',
              'description' => 'Title',
              'examples' => [
                'Test 1',
                'Test 2',
              ],
            ],
            'test-slot-only-required' => [
              'title' => 'test',
            ],
          ],
          'js' => [
            'original' => 'console.log("Test")',
            'compiled' => 'console.log("Test")',
          ],
          'css' => [
            'original' => '.test { display: none; }',
            'compiled' => '.test{display:none;}',
          ],
        ],
        [],
      ],
      'Valid: empty JS and CSS, no props, and "disabled"' => [
        [
          'machineName' => 'test-no-js-no-css-no-props-nor-slots-and-disabled',
          'status' => FALSE,
          'name' => 'Test',
          'props' => [],
          'slots' => [],
          'js' => [
            'original' => '',
            'compiled' => '',
          ],
          'css' => [
            'original' => '',
            'compiled' => '',
          ],
        ],
        [],
      ],
      'Valid: image prop' => [
        [
          'machineName' => 'image-prop-no-slots',
          'name' => 'Test',
          'props' => [
            'image' => [
              'title' => 'Image title',
              'type' => 'object',
              '$ref' => "json-schema-definitions://experience_builder.module/image",
              'examples' => [
                [
                  'src' => 'https://example.com/image.png',
                  'alt' => 'Alternative text',
                  'width' => 800,
                  'height' => 600,
                ],
              ],
            ],
          ],
          'slots' => [],
          'js' => [
            'original' => 'console.log("Test")',
            'compiled' => 'console.log("Test")',
          ],
          'css' => [
            'original' => '.test { display: none; }',
            'compiled' => '.test{display:none;}',
          ],
        ],
        [],
      ],
      'Invalid: required image prop missing examples' => [
        [
          'machineName' => 'image-prop-no-slots-no-examples',
          'name' => 'Test',
          'required' => [
            'image',
          ],
          'props' => [
            'image' => [
              'title' => 'Image title',
              'type' => 'object',
              '$ref' => "json-schema-definitions://experience_builder.module/image",
            ],
          ],
          'slots' => [],
          'js' => [
            'original' => 'console.log("Test")',
            'compiled' => 'console.log("Test")',
          ],
          'css' => [
            'original' => '.test { display: none; }',
            'compiled' => '.test{display:none;}',
          ],
        ],
        [
          '' => 'Prop "image" is required, but does not have example value',
        ],
      ],
      'Valid: optional image prop missing examples' => [
        [
          'machineName' => 'image-prop-no-slots-no-examples',
          'name' => 'Test',
          'props' => [
            'image' => [
              'title' => 'Image title',
              'type' => 'object',
              '$ref' => "json-schema-definitions://experience_builder.module/image",
            ],
          ],
          'slots' => [],
          'js' => [
            'original' => 'console.log("Test")',
            'compiled' => 'console.log("Test")',
          ],
          'css' => [
            'original' => '.test { display: none; }',
            'compiled' => '.test{display:none;}',
          ],
        ],
        [],
      ],
      'Invalid: image prop $ref' => [
        [
          'machineName' => 'image-prop-no-slots-no-ref',
          'name' => 'Test',
          'props' => [
            'image' => [
              'title' => 'Image title',
              'type' => 'object',
              'examples' => [
                // @todo this is actually an invalid example, will be detected by https://www.drupal.org/i/3508725
                'src' => 'https://example.com/image.png',
                'alt' => 'Alternative text',
                'width' => 800,
                'height' => 600,
              ],
            ],
          ],
          'slots' => [],
          'js' => [
            'original' => 'console.log("Test")',
            'compiled' => 'console.log("Test")',
          ],
          'css' => [
            'original' => '.test { display: none; }',
            'compiled' => '.test{display:none;}',
          ],
        ],
        [
          '' => 'Experience Builder does not know of a field type/widget to allow populating the <code>image</code> prop, with the shape <code>{"type":"object"}</code>.',
          'props.image' => '\'$ref\' is a required key because props.image.type is object (see config schema type experience_builder.json_schema.prop.object).',
        ],
      ],
      'Invalid: image prop with incorrect $ref' => [
        [
          'machineName' => 'test-props-no-slots',
          'name' => 'Test',
          'props' => [
            'image' => [
              'title' => 'Image title',
              'type' => 'object',
              '$ref' => "json-schema-definitions://experience_builder.module/textarea",
              'examples' => [
                [
                  'src' => 'https://example.com/image.png',
                  'alt' => 'Alternative text',
                  'width' => 800,
                  'height' => 600,
                ],
              ],
            ],
          ],
          'slots' => [],
          'js' => [
            'original' => 'console.log("Test")',
            'compiled' => 'console.log("Test")',
          ],
          'css' => [
            'original' => '.test { display: none; }',
            'compiled' => '.test{display:none;}',
          ],
        ],
        [
          '' => 'Experience Builder does not know of a field type/widget to allow populating the <code>image</code> prop, with the shape <code>{"type":"object","$ref":"json-schema-definitions://experience_builder.module/textarea"}</code>.',
          'props.image.$ref' => 'The value you selected is not a valid choice.',
        ],
      ],
      'Valid: textarea prop with $ref' => [
        [
          'machineName' => 'test-props-no-slots',
          'name' => 'Test',
          'props' => [
            'textarea' => [
              'title' => 'Textarea',
              'type' => 'string',
              '$ref' => "json-schema-definitions://experience_builder.module/textarea",
              'examples' => [
                'Simple',
                'Example',
              ],
            ],
          ],
          'slots' => [],
          'js' => [
            'original' => 'console.log("Test")',
            'compiled' => 'console.log("Test")',
          ],
          'css' => [
            'original' => '.test { display: none; }',
            'compiled' => '.test{display:none;}',
          ],
        ],
        [],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function providerInvalidMachineNameCharacters(): array {
    return [
      'INVALID: space separated' => ['space separated', FALSE],
      'INVALID: period separated' => ['period.separated', FALSE],
      'VALID: dash separated' => ['dash-separated', TRUE],
      'VALID: underscore separated' => ['underscore_separated', TRUE],
      'VALID: contains uppercase' => ['containsUppercase', TRUE],
      'INVALID: starts uppercase' => ['StartsUppercase', FALSE],
      'VALID: contains number' => ['number1', TRUE],
      'INVALID: starts with number' => ['10th_birthday', FALSE],
    ];
  }

  public function testInvalidSlotIdentifiedByConfigSchema(): void {
    $original_test_slot = $this->entity->get('slots')['test-slot'];
    $this->entity->set('slots', [
      '0-slot' => $original_test_slot,
    ]);
    // @todo This test case should have validation errors because '0-slot' is not a valid slot name.
    //   But currently we can not use the 'patternProperties' until
    //   https://www.drupal.org/i/3471064 is fixed.
    $this->assertValidationErrors([]);
    $this->entity->set('slots', ['test-slot' => []]);
    $this->assertValidationErrors([
      'slots.test-slot' => "'title' is a required key.",
    ]);
  }

  public function testCollisionBetweenPropsAndSlots(): void {
    $prop_colliding_with_slot = [
      'test-slot' => [
        'title' => 'contrived example',
        'type' => 'string',
        'examples' => ['foo'],
      ],
    ];
    $this->entity->set('props', $prop_colliding_with_slot);
    $this->assertValidationErrors([
      '' => 'The component "experience_builder:test" declared [test-slot] both as a prop and as a slot. Make sure to use different names.',
    ]);

    // Verify that if there's a lower-level problem, that both the low-level and
    // this high-level consistency validation error appear.
    unset($prop_colliding_with_slot['test-slot']['examples']);
    $this->entity->set('props', $prop_colliding_with_slot);
    $this->assertValidationErrors([
      '' => 'The component "experience_builder:test" declared [test-slot] both as a prop and as a slot. Make sure to use different names.',
    ]);
  }

  protected function assertValidationErrors(array $expected_messages): void {
    // JsComponentHasValidAndSupportedSdcMetadata adds additional validation, but
    // \Drupal\KernelTests\Core\Config\ConfigEntityValidationTestBase::testInvalidMachineNameCharacters()
    // does not provide a way to add additional errors when the machine name is
    // invalid.
    $invalid_id_messages = [
      'machineName' => 'The <em class="placeholder">&quot;' . $this->entity->id() . '&quot;</em> machine name is not valid.',
      '' => "The 'machineName' property cannot be changed.",
    ];
    // 'dash-separated' is valid machine name for component but not for config
    // entity.
    if ($this->entity->id() !== 'dash-separated' && $expected_messages === $invalid_id_messages) {
      $separator = '/n';
      if (\version_compare(\Drupal::VERSION, '11.1.2', '>=')) {
        // The format of component violation messages changed in Drupal 11.1.2.
        // @see https://drupal.org/i/3462700
        $separator = "\n";
      }
      $expected_messages[''] = [
        '[id] Does not match the regex pattern ^[a-z]([a-zA-Z0-9_-]*[a-zA-Z0-9])*:[a-z]([a-zA-Z0-9_-]*[a-zA-Z0-9])*$' . $separator . '[machineName] Does not match the regex pattern ^[a-z]([a-zA-Z0-9_-]*[a-zA-Z0-9])*$',
        $expected_messages[''],
      ];
    }
    parent::assertValidationErrors($expected_messages);
  }

}
